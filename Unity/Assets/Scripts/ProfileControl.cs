﻿using UnityEngine;
using System.Collections;

public class ProfileControl : MonoBehaviour {
	GameObject mainPage;
	public GameObject GoTheteam;
	public GameObject editProfile;
	int playerAssessId;
	bool profileIsOn;


	void Start()
	{
		profileIsOn = false;
	}

	public void PlayerProfileControl()
	{	
		playerAssessId = GoTheteam.GetComponent<PlayerAssessment>().playerAssessId;
		
		mainPage = GameObject.Find("1.Main") as GameObject;
		mainPage.GetComponent<Main>().playerAssessId = playerAssessId;
		
		
		
		mainPage.GetComponent<Main>().PlayerProfileControl();
	}
	
	public void CloseProfile()
	{
		editProfile.SetActive(false);
	}
}
