﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Data;
using System.Text;
using Mono.Data.SqliteClient;

public class Comments : MonoBehaviour {
	private SqliteConnection db = null;
	private string connection;
	
	public UILabel comments;
	public string theComment;
	
	public UILabel thePlayerNumber;
	
	public int currentMatch;
	public int scoutId;
	public int playerNo;
	public int playerAssessId;
	public string team;
	
	void Start ()
	{
		playerNo = int.Parse(thePlayerNumber.text);
	
		scoutId = PlayerPrefs.GetInt ("ScoutId");
		currentMatch = PlayerPrefs.GetInt ("CurrentMatch");
		
		SetText();
	}

	public void SetText()
	{
//		print (scoutId);
//		print (playerNo);
//		print (currentMatch);
//		print (team);
	
		playerNo = int.Parse(thePlayerNumber.text);
		ConnectSql ();
		try {
			
			string sql = "SELECT * FROM assessments WHERE matchId = '"+ currentMatch +"' AND scoutId = '"+ scoutId + "' AND positionNumber = '"+ playerNo + "' AND team = '"+ team +"'";
			
			SqliteDataReader reader = Exicute(sql);
			
			if (reader.HasRows) {
				int i = 0;
				
				while (reader.Read()) {
					if(i==0){
						if (reader ["comments"] != null){
							comments.text = reader ["comments"].ToString ();
						}
					}
					
					i++;	
				}
				
			} else {
				// ADD TEAMS
			}
			reader.Close ();
			reader = null;
			CloseConnection();
		} catch {
			print ("Not Connected");
		}
	}
	
	private bool ExicuteQuery(string sql)
	{
		ConnectSql();
		try{
			SqliteCommand cmd = new SqliteCommand (sql , db);
			int rowId = cmd.ExecuteNonQuery ();
			if (rowId > 0)
			{
				return true;
			}else{
				return false;
			}
			
		}catch {
			print ("Query not exicuted");
			return false;
		}
		
		
		
		CloseConnection();
	}
	
	
	
	// read data
	private SqliteDataReader Exicute(string sql){
		ConnectSql ();
		try {
			SqliteCommand cmd = new SqliteCommand (sql, db);
			SqliteDataReader reader = cmd.ExecuteReader ();
			
			return reader;
			
		}catch {
			print ("Not Connected");
			return null;
		}
		CloseConnection();
	}
	
	public void ConnectSql(){
		string p = "ScoutDataTest2.db";
		string filepath = Application.persistentDataPath + "/" + p;
		
		//print (filepath);
		
		if(!File.Exists(filepath))
		{
			// if it doesn't ->
			// open StreamingAssets directory and load the db ->
			WWW loadDB = new WWW("jar:file://" + Application.dataPath + "!/assets/" + p);  // this is the path to your StreamingAssets in android
			while(!loadDB.isDone) {}  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
			
			// then save to Application.persistentDataPath
			
			File.WriteAllBytes(filepath, loadDB.bytes);
			
		}
		
		//open db connection
		connection = "URI=file:" + filepath;
		db = new SqliteConnection(connection);
		db.Open();
	}
	
	public void CloseConnection(){
		db.Close ();
		db = null;
	}
	
	public void ClearThetext()
	{
		comments.text = "";
	}
}
