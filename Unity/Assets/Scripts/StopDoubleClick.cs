﻿using UnityEngine;
using System.Collections;

public class StopDoubleClick : MonoBehaviour {
	public UIButton theButton;
	public UILabel text;
	public string origilnalText;
	public int waitTime;


	public void StopDouble () {
		StartCoroutine (Stopped ());
	}

	IEnumerator Stopped()
	{
		text.text = "LOADING...";
		theButton.isEnabled = false;
		yield return new WaitForSeconds(waitTime);
		text.text = origilnalText;
		theButton.isEnabled = true;
	}

}
