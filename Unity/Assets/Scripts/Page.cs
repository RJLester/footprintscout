﻿using UnityEngine;
using System.Collections;

public class Page : MonoBehaviour 
{
	public virtual void AnimatePageIn()
	{
		//print ("PageIn: "+gameObject.name);
		gameObject.SetActive(true);
		//print ("Animate Page In");
		transform.localPosition = new Vector3(0, 0); //(Screen.width*2.5f, 0);
		if(!gameObject.activeSelf) gameObject.SetActive(true);
		
		iTween.MoveTo(gameObject, iTween.Hash("position", new Vector3(0f, 0f, 0f), "islocal", true, "time", 0.0f, "easetype", iTween.EaseType.easeInOutQuad,
		                                      "oncomplete", "StartPageContent", "oncompletetarget", gameObject));
	}
	
	///<summary>This is called after the Page has finished moving into place</summary>
	protected virtual void StartPageContent()
	{
		//print ("StartPageContent()");
		BroadcastMessage("StartUp",SendMessageOptions.DontRequireReceiver);	
	}
	
	public virtual void AnimatePageOut()
	{
		//print ("PageOut: "+gameObject.name);
		//Vector3 moveToX = pageBG != null? new Vector3(-1920, 0): new Vector3(-Screen.width*2.5f, 0);
		Vector3 moveToX = new Vector3(0, 0); //(-Screen.width*2.5f, 0);
		iTween.MoveTo(gameObject, iTween.Hash("position", moveToX, "islocal", true, "time", 0.0f, "easetype", iTween.EaseType.easeInOutQuad,
		                                      "oncomplete", "PageOutComplete", "oncompletetarget", gameObject));
	}
	
	
	///<summary>This is called after the Page has finished moving into place</summary>
	protected virtual void PageOutComplete()
	{

		//gameObject.SetActive(false);
		DestroyObject(gameObject);

	}

}
