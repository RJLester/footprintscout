﻿using UnityEngine;
using System.Collections;

public class Display : MonoBehaviour 
{
	public static Display instance;

	public Page pageIn{get;set;}
	public Page pageOut{get;set;}

	public GameObject pageHolder;
	//public GameObject prefabGotoPage;
	GameObject mainGameObject;
	
	void Awake()
	{
		instance = this;
	}

	void Start()
	{
		if (PlayerPrefs.GetInt("IsLoggedIn") != 1)
		{
			// Loads Login Screen. Only runs once for starting.
			ChangePages("LoginScreen");
			
		}else
		{
			ChangePages("Profiles");
			mainGameObject = GameObject.Find("1.Main");
			mainGameObject.GetComponent<Main>().Login();
		}
	}

	public void ChangePages(string pageTo){
		if(pageHolder.transform.childCount > 0)
		{
			pageHolder.transform.GetChild(0).GetComponent<Page>().AnimatePageOut();
		}

		GameObject p = NGUITools.AddChild(pageHolder, (Resources.Load(pageTo) as GameObject));
		Page pageIn = p.GetComponent<Page>();
		pageIn.AnimatePageIn();
	}
}
