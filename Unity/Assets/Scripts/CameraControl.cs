﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Data;
using System.Text;
using Mono.Data.SqliteClient;

public class CameraControl : MonoBehaviour {
	//string url = "http://upload.wikimedia.org/wikipedia/commons/thumb/1/15/Red_Apple.jpg/220px-Red_Apple.jpg";
	public GameObject photoHolder;
	string playerAssessId = "92";
	private SqliteConnection db = null;
	private string connection;
	
	String b64_string;
	
	void Start()
	{
		GetImage();
		
		byte[] b64_bytes = System.Convert.FromBase64String(b64_string); 
		Texture2D tex = new Texture2D(1,1);
		tex.LoadImage(b64_bytes);
		tex.wrapMode = TextureWrapMode.Repeat;
		
		tex.mipMapBias = -0.5f;
		photoHolder.renderer.material.mainTexture = tex;
	}
	
	
	
	public void GetImage () {
		
		
		ConnectSql ();
		try {
			
			string sql = "SELECT * FROM assessments WHERE assessId = '"+ playerAssessId +"'";
			//string sql = "SELECT * FROM Teams WHERE TeamID = 2";
			
			SqliteDataReader reader = Exicute(sql);
			
			if (reader.HasRows) {
				//print (playerNo);
				int i = 0;
				
				
				while (reader.Read()) {
					if(i==0){
						if (reader ["images"] != null){
							//textBox.value = reader ["comments"].ToString ();
							
							b64_string = reader ["images"].ToString ();
							print (b64_string);
						}
					}
					
					i++;	
				}
				
			} else {
				// ADD TEAMS
			}
			reader.Close ();
			reader = null;
			
			CloseConnection();
		} catch {
			print ("Not Connected");
		}
		
	}


	private bool ExicuteQuery(string sql)
	{
		ConnectSql();
		try{
			SqliteCommand cmd = new SqliteCommand (sql , db);
			int rowId = cmd.ExecuteNonQuery ();
			if (rowId > 0)
			{
				return true;
			}else{
				return false;
			}
			
		}catch {
			print ("Query not exicuted");
			return false;
		}
		
		
		
		CloseConnection();
	}
	
	
	
	// read data
	private SqliteDataReader Exicute(string sql){
		ConnectSql ();
		try {
			SqliteCommand cmd = new SqliteCommand (sql, db);
			SqliteDataReader reader = cmd.ExecuteReader ();
			
			return reader;
			
		}catch {
			print ("Not Connected");
			return null;
		}
		CloseConnection();
	}
	
	public void ConnectSql(){
		string p = "ScoutDataTest2.db";
		string filepath = Application.persistentDataPath + "/" + p;
		
		//print (filepath);
		
		if(!File.Exists(filepath))
		{
			// if it doesn't ->
			// open StreamingAssets directory and load the db ->
			WWW loadDB = new WWW("jar:file://" + Application.dataPath + "!/assets/" + p);  // this is the path to your StreamingAssets in android
			while(!loadDB.isDone) {}  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
			
			// then save to Application.persistentDataPath
			
			File.WriteAllBytes(filepath, loadDB.bytes);
			
		}
		
		//open db connection
		connection = "URI=file:" + filepath;
		db = new SqliteConnection(connection);
		db.Open();
	}
	
	public void CloseConnection(){
		db.Close ();
		db = null;
	}
	
	void LoadTheTexture()
	{
		
	}
}
