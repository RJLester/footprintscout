﻿using UnityEngine;
using System.Collections;

public class MatchesDropDownBtn : MonoBehaviour {
	public GameObject theButtons;
	public GameObject background;
	
	bool buttonsOn = false;
	
	public void MatchButtonsOffOn()
	{
		if(!buttonsOn)
		{
			theButtons.SetActive(true);
			background.SetActive(true);
			buttonsOn = true;
		}
		else{
			theButtons.SetActive(false);
			background.SetActive(false);
			buttonsOn = false;
		}
	}
}
