﻿using UnityEngine;
using System.Collections;

public class CommetsControl : MonoBehaviour {
	GameObject mainPage;
	public GameObject GoTheteam;
	//public GameObject editComments;
	
	int playerAssessId;
	
	bool commentsIsOn;
	
	
	void Start()
	{
		commentsIsOn = false;
	}
	
	public void CommentsControl()
	{	
		playerAssessId = GoTheteam.GetComponent<PlayerAssessment>().playerAssessId;
		
		mainPage = GameObject.Find("1.Main") as GameObject;
		mainPage.GetComponent<Main>().playerAssessId = playerAssessId;
		
		
		
		mainPage.GetComponent<Main>().CommentsControl();
	}
	
	public void CloseProfile()
	{
		//editComments.SetActive(false);
	}
}
