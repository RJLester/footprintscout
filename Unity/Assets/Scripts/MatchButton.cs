﻿using UnityEngine;
using System.Collections;

public class MatchButton : Page {
	public UILabel buttonTxt;
	public int matchID;

	public void LoadAssessment()
	{

		PlayerPrefs.SetInt ("CurrentMatch", matchID);

		Display.instance.ChangePages ("Assesment");
		//print (PlayerPrefs.GetInt("CurrentMatch"));
	}                        
}
