﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Data;
using System.Text;
using Mono.Data.SqliteClient;


public class PlayerAssessment : MonoBehaviour {
	public GameObject go_Assessments;

	public int currentMatch;
	public int scoutId;
	public int playerNo;
	public int playerAssessId;
	
	public int theTeam; // just for testing
	//public string team;
	
	// Player Stats
	// Player Details
	public string playerName;
	public string surname;
	public string id;
	public string union;
	public string leauge;
	public string team;
	public string position;
	public string age;
	
	// Player team1 details
	public UILabel lableName;
	public UILabel lableSurname;
	public UILabel lablePosition;
	public UILabel lableAge;
	public UILabel lableComments;
	
	public UISlider uISlider_mental;
	public UISlider uISlider_law;
	public UISlider uISlider_cond;
	public UISlider uISlider_attack;
	public UISlider uISlider_def;
	public UISlider uISlider_kickGame;
	public UISlider uISlider_breakDown;
	public UISlider uISlider_scrum;
	public UISlider uISlider_lineOut;
	public UISlider uISlider_kickRecieve;
	
	// read, write and save data
	private SqliteConnection db = null;
	public string filePath;
	public string result;
	private string connection;
	private IDbConnection dbcon;
	private IDbCommand dbcmd;
	private IDataReader reader;
	private StringBuilder builder;
	
	public UILabel lablePlayerNumber;

	void Start()
	{
		//print(playerNo);
		playerNo = 1;
		//print(playerNo);
		
		scoutId = PlayerPrefs.GetInt ("ScoutId");
		currentMatch = PlayerPrefs.GetInt ("CurrentMatch");

		if(gameObject.name == "Team1")
		{
			team = go_Assessments.GetComponent<Assessments>().team1Name;
			go_Assessments.GetComponent<Assessments>().theTeam = team;
		}else
		{
			team = go_Assessments.GetComponent<Assessments>().team2Name;
			go_Assessments.GetComponent<Assessments>().theTeam = team;
		}
		
		lablePlayerNumber.text = "1";
		
		
		ChangePlayer();
	}

	public void ChangePlayer()
	{
//		print(currentMatch);
//		print(scoutId);
//		print(playerNo);
//		print(team);
		
		
//		
//		uISlider_law.value = 0;
//		uISlider_cond.value = 0;
//		uISlider_attack.value = 0;
//		uISlider_def.value = 0;
//		uISlider_kickGame.value = 0;
//		uISlider_breakDown.value = 0;
//		uISlider_scrum.value = 0;
//		uISlider_lineOut.value = 0;
//		uISlider_kickRecieve.value = 0;
		
		
		GetAssessment();
		lablePlayerNumber.text = playerNo.ToString();
		
		
	}
	
	List <Details> assessments;
	public struct Details
	{
		// Player Details
		public string PlayerName;
 		public string PlayerSurname;
		public string PlayerPosition;
 		public int PlayerAge;
 		public int PlayerAssessId;
 		
		public int mental;
		public int law;
		public int cond;
		public int attack;
		public int def;
		public int kickGame;
		public int breakDown;
		public int scrum;
		public int lineOut;
		public int kickRecieve;
	}
	
	public void CreateAssessment()
	{
	
	}
	
	public void GetAssessment()
	{
		
		
		if (assessments == null)
			assessments = new List<Details> ();
		if (assessments.Count > 0)
			assessments.Clear ();
		
		//db = new SqliteConnection ("URI=file:C:/wamp/www/ScoutData.db");
		
		ConnectSql ();
		try {
			
			string sql = "SELECT * FROM assessments WHERE matchId = '"+ currentMatch +"' AND scoutId = '"+ scoutId + "' AND positionNumber = '"+ playerNo + "' AND team = '"+ team +"'";
			//string sql = "SELECT * FROM Teams WHERE TeamID = 2";
			
			SqliteDataReader reader = Exicute(sql);
			
			if (reader.HasRows) {
				//print (playerNo);
			int i = 0;
				lableName.text = "Name";
				lableSurname.text = "Surname";
				lablePosition.text = "Position";
				lableAge.text = "Age";
				lableComments.text = "Enter Comments";
				
				uISlider_mental.value = 0;
				
				
				while (reader.Read()) {
				if(i==0){
					if (reader ["playerName"] != null){
						lableName.text = reader ["playerName"].ToString ();
					}
					if (reader ["playerSurname"] != null){
						lableSurname.text = reader ["playerSurname"].ToString ();
					}	
					if (reader ["position"] != null){
						lablePosition.text = reader ["position"].ToString ();
					}
					if (reader ["age"] != null){
						lableAge.text = reader ["age"].ToString ();
					}
					if (reader ["assessId"] != null){
						playerAssessId = int.Parse (reader ["assessId"].ToString ());
					}
					
					if (reader ["comments"] != null){
							lableComments.text = reader ["comments"].ToString ();
					}
			
						if (reader ["mantalApp"] != null){
						
							uISlider_mental.value = (float.Parse (reader ["mantalApp"].ToString ())) / 10;
							//print ("Not Zero");
						}
						else
						{
							uISlider_mental.value = 0;
							//print ("Zero");
						}
						
						if (reader ["lawApp"] != null){
							uISlider_law.value = (float.Parse (reader ["lawApp"].ToString ())) / 10;
						}
						else
						{
							uISlider_law.value = 0;
						}
						
						if (reader ["conditioning"] != null){
							uISlider_cond.value = (float.Parse (reader ["conditioning"].ToString ())) / 10;
						}
						else
						{
							uISlider_cond.value = 0;
						}
						
						if (reader ["attack"] != null){
							uISlider_attack.value = (float.Parse (reader ["attack"].ToString ())) / 10;
						}
						else
						{
							uISlider_attack.value = 0;
						}
						if (reader ["defence"] != null){
							uISlider_def.value = (float.Parse (reader ["defence"].ToString ())) / 10;
						}
						if (reader ["kickingGame"] != null){
							uISlider_kickGame.value = (float.Parse (reader ["kickingGame"].ToString ())) / 10;
						}
						if (reader ["breakDown"] != null){
							uISlider_breakDown.value = (float.Parse (reader ["breakDown"].ToString ())) / 10;
						}
						if (reader ["scrum"] != null){
							uISlider_scrum.value = (float.Parse (reader ["scrum"].ToString ())) / 10;
						}
						if (reader ["lineout"] != null){
							uISlider_lineOut.value = (float.Parse (reader ["lineout"].ToString ())) / 10;
						}
						if (reader ["kickRecieved"] != null){
							uISlider_kickRecieve.value = (float.Parse (reader ["kickRecieved"].ToString ())) / 10;
						}
				}
					
					i++;	
				
				}
//				while (reader.Read()) {
//					Details l = new Details ();	
//					if (reader ["playerName"] != null){
//						l.PlayerName = reader ["playerName"].ToString ();
//					}	
					
//					
//					assessments.Add (  l  );
//				}


				reader.Close ();
				reader = null;
				CloseConnection();
			} else {
			
			
				
				// ADD ASSEMENT
				sql = "INSERT INTO assessments (matchId,scoutId,positionNumber,team) VALUES('"+ currentMatch +"','"+ scoutId + "', '"+ playerNo + "','"+ team +"') ;";
				SqliteCommand cmd = new SqliteCommand (sql, db);
				int r= cmd.ExecuteNonQuery();
				
				if(r>0){
					GetAssessment();
				}
			}
			
		} catch {
			print ("Not Connected");
		}
	}	
	
	
	
	// read data
	private SqliteDataReader Exicute(string sql){
		ConnectSql ();
		try {
			SqliteCommand cmd = new SqliteCommand (sql, db);
			SqliteDataReader reader = cmd.ExecuteReader ();
			
			return reader;
			
		}catch {
			print ("Not Connected");
			return null;
		}
		CloseConnection();
	}
	
	public void ConnectSql(){
		string p = "ScoutDataTest2.db";
		string filepath = Application.persistentDataPath + "/" + p;
		
		//print (filepath);
		
		if(!File.Exists(filepath))
		{
			// if it doesn't ->
			// open StreamingAssets directory and load the db ->
			WWW loadDB = new WWW("jar:file://" + Application.dataPath + "!/assets/" + p);  // this is the path to your StreamingAssets in android
			while(!loadDB.isDone) {}  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
			
			// then save to Application.persistentDataPath
			
			File.WriteAllBytes(filepath, loadDB.bytes);
			
		}
		
		//open db connection
		connection = "URI=file:" + filepath;
		db = new SqliteConnection(connection);
		db.Open();
	}
	
	public void CloseConnection(){
		db.Close ();
		db = null;
	}
	
}
