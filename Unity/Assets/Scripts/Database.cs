using UnityEngine;
using System.Collections;
using Mono.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.IO;

public class Database : MonoBehaviour {
	public GameObject sync;
	
	int reader;
	
	private string connection;
	private SqliteConnection db = null;
	public void ConnectDatabase()
	{
		string p = "ScoutDataTest2.db";
		string filepath = Application.persistentDataPath + "/" + p;
		
		print (filepath);
		
		if(!File.Exists(filepath))
		{
			// if it doesn't ->
			// open StreamingAssets directory and load the db ->
			WWW loadDB = new WWW("jar:file://" + Application.dataPath + "!/assets/" + p);  // this is the path to your StreamingAssets in android
			while(!loadDB.isDone) {}  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
			
			// then save to Application.persistentDataPath
			
			File.WriteAllBytes(filepath, loadDB.bytes);
			
		}
		
		//open db connection
		connection = "URI=file:" + filepath;
		db = new SqliteConnection(connection);
		db.Open();
	}

	void OnClick()
	{
		string collectData = updateServer ();
		sendData (collectData);

	}


	public void CloseDatabase()
	{
		db.Close();
		db = null;
	}

	public string updateServer()
	{
		
		string json = "[{";
		SqliteDataReader matches = getData("SELECT * FROM matches" );
			
			int i =0;
			if (matches.HasRows) {
				json = json+"\"matches\":[";
			string c = ",";
				while(matches.Read()) {
				c = ",";
				if(i==0){
					c ="";
				}
				json = json+c+"{\"matchId\":\""+(matches["matchid"].ToString())+"\"," +
							  "\"scoutId\":\""+(matches["scoutId"].ToString())+"\"," +
							  "\"homeTeam\":\""+matches["homeTeam"].ToString()+"\"," +
							  "\"awayTeam\":\""+matches["awayTeam"].ToString()+"\"," +
							  "\"venu\":\""+matches["venu"].ToString()+"\"," +
							  "\"city\":\""+matches["city"].ToString()+"\"," +
							  "\"ageGroup\":\""+(matches["ageGroup"].ToString())+"\"," +
							  "\"conditions\":\""+matches["conditions"].ToString()+"\"," +
						 	  "\"dateTime\":\""+matches["dateTime"].ToString()+"\"," +
							  "\"homeHalfTime\":\""+matches["homeHalfTime"].ToString()+"\"," +
							  "\"homeFullTime\":\""+matches["homeFullTime"].ToString()+"\"," +
							  "\"awayHalfTime\":\""+matches["awayHalfTime"].ToString()+"\","+
							  "\"awayFullTime\":\""+matches["awayFullTime"].ToString()+"\"}" ;
				i++;
				}
				
				json = json+"]";
				}
				
				matches.Close();
				matches = null;
		CloseDatabase();
		
		SqliteDataReader assesments = getData("SELECT * FROM assessments" );
		int e =0;
			if (assesments.HasRows) {
				json = json+",\"assesments\":[";
			string d = ",";
				while(assesments.Read()) {
				d = ",";
				if(e==0){
					d ="";
				}
				
				json = json+d+
								"{\"assessId\":\""+assesments["assessId"].ToString()+"\"," +
							  	"\"playerId\":\""+assesments["playerId"].ToString()+"\"," +
								"\"matchId\":\""+assesments["matchId"].ToString()+"\"," +
								"\"scoutId\":\""+(assesments["scoutId"].ToString())+"\"," +
							  	"\"playerName\":\""+assesments["playerName"].ToString()+"\"," +
							  	"\"playerSurname\":\""+assesments["playerSurname"].ToString()+"\"," +
							  	"\"team\":\""+assesments["team"].ToString()+"\"," +
							  	"\"id\":\""+assesments["id"].ToString()+"\"," +
								"\"age\":\""+assesments["age"].ToString()+"\"," +
								"\"devision\":\""+assesments["devision"].ToString()+"\"," +
								"\"ageGroup\":\""+assesments["ageGroup"].ToString()+"\"," +
								"\"position\":\""+assesments["position"].ToString()+"\"," +
						 		"\"positionNumber\":\""+assesments["positionNumber"].ToString()+"\"," +
								"\"mantalApp\":\""+assesments["mantalApp"].ToString()+"\"," +
								"\"lawApp\":\""+assesments["lawApp"].ToString()+"\"," +
								"\"conditioning\":\""+assesments["conditioning"].ToString()+"\"," +
								"\"attack\":\""+assesments["attack"].ToString()+"\"," +
								"\"defence\":\""+assesments["defence"].ToString()+"\"," +
								"\"kickingGame\":\""+assesments["kickingGame"].ToString()+"\"," +
								"\"breakDown\":\""+assesments["breakDown"].ToString()+"\"," +
								"\"scrum\":\""+assesments["scrum"].ToString()+"\"," +
								"\"lineout\":\""+assesments["lineout"].ToString()+"\"," +
								"\"kickRecieved\":\""+assesments["kickRecieved"].ToString()+"\"," +
								"\"comments\":\""+assesments["comments"].ToString()+"\"}" ;
				e++;
				}
				
				json = json+"]";
				}
				json = json+"}]";
				assesments.Close();
				assesments = null;
		
		CloseDatabase();	
		return json;
		
	}
	

	public int insertRow(string q)
	{
		ConnectDatabase();
		int check = 0;
		try{
			SqliteCommand cmd = new SqliteCommand(q, db);
			reader = cmd.ExecuteNonQuery();
			
		}catch(Exception e){
			print(e.ToString());
		}
		CloseDatabase();
		return check;
		
	}
	
	public bool checkRow(string q)
	{
		ConnectDatabase();
		bool check = false;
		try{
			SqliteCommand cmd = new SqliteCommand(q, db);
			SqliteDataReader reader = cmd.ExecuteReader();
			
			
			if (reader.HasRows) {
					check = true;
				}
				
				reader.Close();
		}catch{
			print("Query Failed!");
		}
		CloseDatabase();
		return check;
		
	}
	
	
	private SqliteDataReader getData( string q ){
		ConnectDatabase();
		SqliteDataReader reader = null;
		try{
			SqliteCommand cmd = new SqliteCommand(q, db);
			reader = cmd.ExecuteReader();
			
		}catch{
			print("Not Connected");
		}
		
		return reader;
	}

	public void sendData(string str)
	{
		
		sync.SetActive(true);
		try{
			
			
			WWWForm form = new WWWForm();
			var headers = form.headers;
			form.AddField( "data", str );
			byte[] rawData = form.data;
			
			string url = "http://footprintapp.net/sync.php";
			//string url = "http://footprintapp.net/putContent.php?data="+data;
	
			WWW www = new WWW(url,form);
			
			StartCoroutine(WaitForRequest(www));
		}catch(Exception e){
			print (e.Message);
		}
	}
	
	
	IEnumerator WaitForRequest(WWW w)
	{
		
		yield return w;

		if (!String.IsNullOrEmpty(w.error))
		{
		print("WWW Error: "+ w.error);
			
		} else {
			print("SEND || "+w.text);
			sync.SetActive(false);
		} 
		
	}
}
