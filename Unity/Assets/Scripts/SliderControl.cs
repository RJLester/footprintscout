﻿using UnityEngine;
using System.Collections;

public class SliderControl : MonoBehaviour {
	public UILabel percentage;
	public UISlider slider;
	public GameObject assessments;
	public GameObject teamGroup;
	
	public bool canChange = false;

	void Start()
	{
		Invoke("TurnOnPercentageChange" , 1);
	}

	void TurnOnPercentageChange()
	{
		canChange = true;
	}

	public void ChangePercentage()
	{
		if(canChange)
		{
			string theValue = (slider.value * 10).ToString ();
			percentage.text = theValue;
	
			//print ("Value Changed");
	
			assessments.GetComponent<Assessments> ().sliderTeam = teamGroup.name;
			assessments.GetComponent<Assessments> ().theSliderName = gameObject.name;
			assessments.GetComponent<Assessments> ().sliderValue = theValue;
			assessments.GetComponent<Assessments> ().SliderControl ();
		
		}
	}
}
