﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Data;
using System.Text;
using Mono.Data.SqliteClient;

public class EditComments : MonoBehaviour {
	public UIInput textBox; 
	
	public int currentMatch;
	public int scoutId;
	int playerAssessId;
	
	public GameObject mainPage;
	
	
	private SqliteConnection db = null;
	private string connection;
	
	public GameObject comments1;
	public GameObject comments2;
	
	public void GetComments () {
		mainPage = GameObject.Find("1.Main");
		playerAssessId = mainPage.GetComponent<Main>().playerAssessId;
		
		ConnectSql ();
		try {
			
			string sql = "SELECT * FROM assessments WHERE assessId = '"+ playerAssessId +"'";
			//string sql = "SELECT * FROM Teams WHERE TeamID = 2";
			
			SqliteDataReader reader = Exicute(sql);
			
			if (reader.HasRows) {
				//print (playerNo);
				int i = 0;
				//playerName.text = "Name";
				//surname.text = "Surname";
				//id.text = "Position";
				//lableAge.text = "Age";
				
				
				while (reader.Read()) {
					if(i==0){
						if (reader ["playerName"] != null){
							textBox.value = reader ["comments"].ToString ();
						}
					}
					
					i++;	
				}

			} else {
				// ADD TEAMS
			}
			reader.Close ();
			reader = null;
			CloseConnection();
		} catch {
			print ("Not Connected");
		}
		
	}
	
	public void UpdateDetails()
	{
		ConnectSql ();
		try {
			////All Teams
			string sql = "SELECT * FROM assessments WHERE assessId = '"+ playerAssessId +"'";
			
			
			// select where
			//string sql = "SELECT * FROM Teams WHERE TeamID = 2";
			
			SqliteDataReader reader = Exicute(sql);
			
			
			if (reader.HasRows) {
				bool ok = ExicuteQuery ("UPDATE assessments SET comments = '"+textBox.value+"' WHERE assessId = '"+ playerAssessId +"'");
				print ("Value updated");
			} else {
				bool ok = ExicuteQuery ("INSERT INTO assessments (comments) VALUES('" + textBox.value + "')");
				print ("Value edited");
			}
			reader.Close ();
			reader = null;
			
			
			
			
		} catch {
			print ("Not Connected");
		}
		
//		if(){
//		
//		}
//		else
//		{
//		
//		}
		//comments1 = GameObject.Find ("Comments1");
		//comments2 = GameObject.Find ("Comments2");
//		
//		comments1.GetComponent<Comments>().playerAssessId = playerAssessId;
//		
		comments1.GetComponent<Comments>().SetText();
		comments2.GetComponent<Comments>().SetText();
		
		Close();
		
		
		
		gameObject.SetActive(false);
	}
	
	private bool ExicuteQuery(string sql)
	{
		ConnectSql();
		try{
			SqliteCommand cmd = new SqliteCommand (sql , db);
			int rowId = cmd.ExecuteNonQuery ();
			if (rowId > 0)
			{
				return true;
			}else{
				return false;
			}
			
		}catch {
			print ("Query not exicuted");
			return false;
		}
		
		
		
		CloseConnection();
	}
	

	
	// read data
	private SqliteDataReader Exicute(string sql){
		ConnectSql ();
		try {
			SqliteCommand cmd = new SqliteCommand (sql, db);
			SqliteDataReader reader = cmd.ExecuteReader ();
			
			return reader;
			
		}catch {
			print ("Not Connected");
			return null;
		}
		CloseConnection();
	}
	
	public void ConnectSql(){
		string p = "ScoutDataTest2.db";
		string filepath = Application.persistentDataPath + "/" + p;
		
		//print (filepath);
		
		if(!File.Exists(filepath))
		{
			// if it doesn't ->
			// open StreamingAssets directory and load the db ->
			WWW loadDB = new WWW("jar:file://" + Application.dataPath + "!/assets/" + p);  // this is the path to your StreamingAssets in android
			while(!loadDB.isDone) {}  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
			
			// then save to Application.persistentDataPath
			
			File.WriteAllBytes(filepath, loadDB.bytes);
			
		}
		
		//open db connection
		connection = "URI=file:" + filepath;
		db = new SqliteConnection(connection);
		db.Open();
	}
	
	public void CloseConnection(){
		db.Close ();
		db = null;
	}


	public void Close()
	{
		comments1 = GameObject.Find ("Comments1");
		comments2 = GameObject.Find ("Comments2");
		
		comments1.GetComponent<Comments>().SetText();
		comments1.GetComponent<Comments>().SetText();
		
		
		ClearTextBox();
		//mainPage.GetComponent<Main>().CommentsControl();
		textBox.value = "Enter Comments";
	}

	public void ClearTextBox () {
		textBox.value = "Enter Comments";
	}
	
	
}
