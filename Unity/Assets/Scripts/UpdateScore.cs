﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Data;
using System.Text;
using Mono.Data.SqliteClient;


public class UpdateScore : MonoBehaviour {
	public UIInput firstTeam1;
	public UIInput firstTeam2;
	public UIInput secondTeam1;
	public UIInput secondTeam2;

	int currentMatch;
	GameObject main;

	private SqliteConnection db = null;
	private string connection;
	
	void Start()
	{
		main = GameObject.Find("1.Main");
		currentMatch = PlayerPrefs.GetInt("CurrentMatch");
		
	}

	public void GetGameScore()
	{
		currentMatch = PlayerPrefs.GetInt("CurrentMatch");
		
		ConnectSql ();
		try {
			
			string sql = "SELECT * FROM matches WHERE matchId = '"+ currentMatch +"'";
			
			SqliteDataReader reader = Exicute(sql);
			
			if (reader.HasRows) {
				int i = 0;

				while (reader.Read()) {
					if(i==0){
						if (reader ["homeHalfTime"] != null){
							firstTeam1.value = reader ["homeHalfTime"].ToString ();
						}
						if (reader ["homeFullTime"] != null){
							firstTeam2.value = reader ["homeFullTime"].ToString ();
						}	
						if (reader ["awayHalfTime"] != null){
							secondTeam1.value = reader ["awayHalfTime"].ToString ();
						}
						if (reader ["awayFullTime"] != null){
							secondTeam2.value = reader ["awayFullTime"].ToString ();
						}
					}
					i++;	
				}

			} else {
				// ADD TEAMS
			}
			reader.Close ();
			reader = null;
			CloseConnection();
		} catch {
			print ("Not Connected");
		}
	}	
	
	
	
	public void AddScore()
	{
		ConnectSql ();
		try {
			////All Teams
			string sql = "SELECT * FROM matches WHERE matchId = '"+ currentMatch +"'";

			
			SqliteDataReader reader = Exicute(sql);
			
			
			if (reader.HasRows) {
				bool ok = ExicuteQuery ("UPDATE matches SET homeHalfTime = '"+firstTeam1.value+"', homeFullTime = '"+firstTeam2.value+"', awayHalfTime = '"+secondTeam1.value+"', awayFullTime = '"+secondTeam2.value+"' WHERE matchId = '"+ currentMatch +"'");
				print ("Value updated");
			} else {
				bool ok = ExicuteQuery ("INSERT INTO matches (homeHalfTime, homeFullTime, awayHalfTime, awayFullTime) VALUES('" + firstTeam1.value + "','" + firstTeam2.value + "','" + secondTeam1.value + "','" + secondTeam2.value + "')");
				print ("Value edited");
			}
			reader.Close ();
			reader = null;
			
			GameObject scoreButton = GameObject.Find("btn_Score");
			scoreButton.GetComponent<ScoreOffOn>().GetGameScore();
			
		} catch {
			print ("Not Connected");
		}
		
	}
	
	private bool ExicuteQuery(string sql)
	{
		ConnectSql();
		try{
			SqliteCommand cmd = new SqliteCommand (sql , db);
			int rowId = cmd.ExecuteNonQuery ();
			if (rowId > 0)
			{
				return true;
			}else{
				return false;
			}
			
		}catch {
			print ("Query not exicuted");
			return false;
		}
		CloseConnection();
	}

	
	// read data
	private SqliteDataReader Exicute(string sql){
		ConnectSql ();
		try {
			SqliteCommand cmd = new SqliteCommand (sql, db);
			SqliteDataReader reader = cmd.ExecuteReader ();
			
			return reader;
			
		}catch {
			print ("Not Connected");
			return null;
		}
		CloseConnection();
	}
	
	public void ConnectSql(){
		string p = "ScoutDataTest2.db";
		string filepath = Application.persistentDataPath + "/" + p;
		
		//print (filepath);
		
		if(!File.Exists(filepath))
		{
			// if it doesn't ->
			// open StreamingAssets directory and load the db ->
			WWW loadDB = new WWW("jar:file://" + Application.dataPath + "!/assets/" + p);  // this is the path to your StreamingAssets in android
			while(!loadDB.isDone) {}  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
			
			// then save to Application.persistentDataPath
			
			File.WriteAllBytes(filepath, loadDB.bytes);
			
		}
		
		//open db connection
		connection = "URI=file:" + filepath;
		db = new SqliteConnection(connection);
		db.Open();
	}
	
	public void CloseConnection(){
		db.Close ();
		db = null;
	}










		
	public void turnOnOff()
	{
		main.GetComponent<Main>().ScoreControl();
	}

	public void ClearAllText()
	{
		firstTeam1.value = "";
		firstTeam2.value = "";
		secondTeam1.value = "";
		secondTeam2.value = "";
	}
}
