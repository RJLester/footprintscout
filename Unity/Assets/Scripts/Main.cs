﻿using UnityEngine;
using System.Collections;
//using System.Collections.Generic;
//using System.Text;
//using System.Xml;
//using System.IO;

public class Main : MonoBehaviour {
	public GameObject staticElements;
	public GameObject createMatch;
	public GameObject playerProfile;
	public GameObject comments;
	public GameObject score;
	public GameObject playerProfileProfliePage;
	
	bool creatMatchIsOn = false;
	public bool profileIsOn = false;
	public bool profileIsOnProfilePage = false;
	bool commentsIsOn = false;
	bool scoreIsOn = false;
	
	public int playerAssessId;

	void Start()
	{
		//To reset login MUST KEEP OFF!
		//PlayerPrefs.SetInt("IsLoggedIn", 0);
	}

	public void Login () {
		Invoke ("TurnOnStatic", 0.5f);
	}

	public void TurnOnStatic(){
		staticElements.SetActive(true);
	}


	public void ScoreControl()
	{
		if (!scoreIsOn) {
			score.SetActive (true);
			scoreIsOn = true;
			score.GetComponent<UpdateScore>().GetGameScore();
		} else {
			score.SetActive(false);
			scoreIsOn = false;
		}
	}
	
	public void MatchControl()
	{
		if (!creatMatchIsOn) {
			createMatch.SetActive (true);
			creatMatchIsOn = true;
		} else {
			createMatch.SetActive(false);
			creatMatchIsOn = false;
		}
	}

	public void PlayerProfileControl()
	{
		//print ("Clicked");

		if (!profileIsOn) {
			playerProfile.SetActive (true);
			
			playerProfile.GetComponent<EditPlayerProfile>().GetPlayerDetails();
			
			profileIsOn = true;
		} else {
			playerProfile.SetActive(false);
			profileIsOn = false;
		}
	}
	
	public void PlayerProfileControlProfilePage()
	{
		//print ("Clicked");
		
		if (!profileIsOnProfilePage) {
			playerProfileProfliePage.SetActive (true);
			
			playerProfileProfliePage.GetComponent<EditPlayerProfileProfilePage>().GetPlayerDetails();
			
			profileIsOnProfilePage = true;
		} else {
			playerProfileProfliePage.SetActive(false);
			profileIsOnProfilePage = false;
		}
	}

	public void CommentsControl()
	{
		if (!commentsIsOn) {
			comments.SetActive (true);
			
			comments.GetComponent<EditComments>().GetComments();
			
			commentsIsOn = true;
		} else {
			comments.SetActive(false);
			commentsIsOn = false;
		}
	}

}
