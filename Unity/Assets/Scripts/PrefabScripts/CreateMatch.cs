﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Data;
using System.Text;
using Mono.Data.SqliteClient;

public class CreateMatch : Page 
{
	public UIInput lableTeam1;
	public UIInput lableTeam2;
	public UIInput lableGrounds;
	public UIInput lableCity;
	public UIInput lableLeague;
	public UIInput lableConditions;

	private string connection;
	private IDbConnection dbcon;
	private IDbCommand dbcmd;
	private IDataReader reader;
	private StringBuilder builder;

	private SqliteConnection db = null;

	public string filePath;
	public string result;

	public void CreateGame()
	{
		string team1 = lableTeam1.value;
		string team2 = lableTeam2.value;
		string grounds = lableGrounds.value;
		string city = lableCity.value;
		string league = lableLeague.value;
		string conditions = lableConditions.value;

		bool ok = ExicuteQuery ("INSERT INTO matches (scoutId, homeTeam, awayTeam, venu, city, ageGroup, conditions, dateTime) VALUES " +
		                        "('" +PlayerPrefs.GetInt("ScoutId")+ "', '" +team1+ "', '" +team2+ "','" +grounds+ "','" +city+ "','" +league+ "','" +conditions+ "','" +System.DateTime.Now.ToString("yyyy-MM-d")+ "');");

		Display.instance.ChangePages ("Profiles");
		//gameObject.SetActive(false);
	}	

	//this is a non query
	private bool ExicuteQuery(string sql)
	{
		ConnectSql();
		try{
			SqliteCommand cmd = new SqliteCommand (sql , db);
			int rowId = cmd.ExecuteNonQuery ();
			if (rowId > 0)
			{
				return true;
			}else{
				return false;
			}
			
		}catch {
			print ("Query not exicuted");
			return false;
		}
		CloseConnection();
	}
	

	public void ConnectSql(){
		string p = "ScoutDataTest2.db";
		string filepath = Application.persistentDataPath + "/" + p;
		
		//print (filepath);
		
		if(!File.Exists(filepath))
		{
			// if it doesn't ->
			// open StreamingAssets directory and load the db ->
			WWW loadDB = new WWW("jar:file://" + Application.dataPath + "!/assets/" + p);  // this is the path to your StreamingAssets in android
			while(!loadDB.isDone) {}  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
			
			// then save to Application.persistentDataPath
			
			File.WriteAllBytes(filepath, loadDB.bytes);
			
		}
		
		//open db connection
		connection = "URI=file:" + filepath;
		db = new SqliteConnection(connection);
		db.Open();

	}
	
	public void CloseConnection(){
		db.Close ();
		db = null;
	}


	public void Cancel()
	{
		gameObject.SetActive(false);
	}

	public void ClearAllText()
	{
		lableTeam1.value = "";
		lableTeam2.value = "";
		lableGrounds.value = "";
		lableCity.value = "";
		lableLeague.value = "";
		lableConditions.value = "";
	}
}

