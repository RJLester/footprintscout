﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Data;
using System.Text;
using Mono.Data.SqliteClient;

public class Profiles : Page 
{
	private SqliteConnection db = null;

	public GameObject PrefabMatchButton;
	public GameObject matchButtonHolder;
	
	public GameObject PrefabPlayerButton;
	public GameObject playerButtonHolder;

	private string connection;
	private IDbConnection dbcon;
	private IDbCommand dbcmd;
	private IDataReader reader;
	private StringBuilder builder;

	void Start () {
		GetMatches ();
		GetPlayers ();
		
		//Debug.Log (Application.dataPath);
		

		//Populate match buttons
		for(int i = 0; i < matches.Count; i++)
		{
			GameObject loadedPrefab = Resources.Load("MatchButton") as GameObject;
			NGUITools.AddChild(matchButtonHolder, loadedPrefab);
			string theText = matches[i].Team1 + " VS " + matches[i].Team2;
			matchButtonHolder.transform.GetChild(i).GetComponent<MatchButton>().buttonTxt.text = theText;
			matchButtonHolder.transform.GetChild(i).GetComponent<MatchButton>().matchID = matches[i].ID;
		}
		
		for(int j = 0; j < TheAssessments.Count; j++)
		{
			GameObject loadedPrefab = Resources.Load("ProfileBox") as GameObject;
			NGUITools.AddChild(playerButtonHolder, loadedPrefab);
			playerButtonHolder.transform.GetChild(j).GetComponent<ProfileBox>().name.text = TheAssessments[j].TheName;
			playerButtonHolder.transform.GetChild(j).GetComponent<ProfileBox>().devision.text = TheAssessments[j].TheDevision;
			playerButtonHolder.transform.GetChild(j).GetComponent<ProfileBox>().ageGroup.text = TheAssessments[j].AgeGroup;
			playerButtonHolder.transform.GetChild(j).GetComponent<ProfileBox>().team.text = TheAssessments[j].TheTeam;
			playerButtonHolder.transform.GetChild(j).GetComponent<ProfileBox>().position.text = TheAssessments[j].Position;
			playerButtonHolder.transform.GetChild(j).GetComponent<ProfileBox>().playerAssessId = TheAssessments[j].PlayerId;
			//print (TheAssessments[j].PlayerId);
		}
	}

	//////////////////////////////////////////////////////////////////
	
	List <ThePlayer> TheAssessments;
	public struct ThePlayer
	{
		public string TheName;
		public string TheDevision;
		public string AgeGroup;
		public string TheTeam;
		public string Position;
		public int PlayerId;
	}
	
	public void GetPlayers()
	{
		if (TheAssessments == null)
			TheAssessments = new List<ThePlayer> ();
		if (TheAssessments.Count > 0)
			TheAssessments.Clear ();
		
		//db = new SqliteConnection ("URI=file:C:/wamp/www/ScoutData.db");
		
		ConnectSql ();
		try {
			////All Teams
			string sql = "SELECT * FROM assessments";
			
			// select where
			//string sql = "SELECT * FROM Teams WHERE TeamID = 2";
			
			//SqliteDataReader theReader = Exicute(sql);
			SqliteCommand cmd = new SqliteCommand (sql, db);
			SqliteDataReader theReader = cmd.ExecuteReader ();
			
			if (theReader.HasRows) {
				while (theReader.Read()) {
					ThePlayer m = new ThePlayer ();
					if (theReader ["playerName"] != null){
						m.TheName = theReader ["playerName"].ToString ();
					}
					
					if (theReader ["devision"] != null){
						m.TheDevision = theReader ["devision"].ToString ();
					}

					if (theReader ["ageGroup"] != null){
						m.AgeGroup = theReader ["ageGroup"].ToString ();
					}
					
					if (theReader ["team"] != null){
						m.TheTeam = theReader ["team"].ToString ();
					}
					
					if (theReader ["positionNumber"] != null){
						m.Position = theReader ["positionNumber"].ToString ();
					}
					
					if (theReader ["assessId"] != null){
						m.PlayerId = int.Parse (theReader ["assessId"].ToString ());
					}

					TheAssessments.Add (  m  );
				}
			} else {
				// ADD TEAMS
			}
			theReader.Close ();
			theReader = null;
			CloseConnection();
		} catch {
			print ("Could not Get Players");
		}
	}	
	


	//////////////////////////////////////////////////////////////////

	List <Match> matches;
	public struct Match
	{
		public int ID;
		public string Team1;
		public string Team2;
	}
	
	public void GetMatches()
	{
		if (matches == null)
			matches = new List<Match> ();
		if (matches.Count > 0)
			matches.Clear ();
		
		//db = new SqliteConnection ("URI=file:C:/wamp/www/ScoutData.db");
		
		ConnectSql ();
		try {
			////All Teams
			string sql = "SELECT * FROM matches";
			
			// select where
			//string sql = "SELECT * FROM Teams WHERE TeamID = 2";
			
			SqliteDataReader reader = Exicute(sql);
			
			if (reader.HasRows) {
				while (reader.Read()) {
					Match l = new Match ();
					
					//if (reader ["playerName"] != null){
						l.ID = int.Parse (reader ["matchID"].ToString ());
					//}
					
					//if (reader ["homeTeam"] != null){
						l.Team1 = reader ["homeTeam"].ToString ();
					//}
					
					//if (reader ["awayTeam"] != null){
						l.Team2 = reader ["awayTeam"].ToString ();
					//}
					
					matches.Add (  l  );
				}
			} else {
				// ADD TEAMS
			}
			reader.Close ();
			reader = null;
			
		} catch {
			print ("Didn't Add Matches");
		}
	}	
	

	/// ///////////////////////////////////////////////////////////////////////////////

	
	// read data
	private SqliteDataReader Exicute(string sql){
		ConnectSql ();
		try {
			SqliteCommand cmd = new SqliteCommand (sql, db);
			SqliteDataReader reader = cmd.ExecuteReader ();
			
			return reader;

		}catch {
			print ("Not Connected");
			return null;
		}
		CloseConnection();
	}

	public void ConnectSql(){
//#if UNITY_ANDROID || UNITY_EDITOR && !UNITY_IPHONE
		string p = "ScoutDataTest2.db";
		string filepath = Application.persistentDataPath + "/" + p;


		if(!File.Exists(filepath))
		{
			// if it doesn't ->
			// open StreamingAssets directory and load the db ->
			WWW loadDB = new WWW("jar:file://" + Application.dataPath + "!/assets/" + p);  		// this is the path to your StreamingAssets in android
			while(!loadDB.isDone) {}  															// CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check

			// then save to Application.persistentDataPath

			File.WriteAllBytes(filepath, loadDB.bytes);

		}
//#endif

//#if UNITY_IPHONE
		//string p = "ScoutDataTest2.db";
		//string filepath = Application.dataPath + "/Raw/" + p;
		//Debug.Log ("File Path" + filepath);
		
//#endif

		connection = "URI=file:" + filepath;
		db = new SqliteConnection(connection);
		db.Open();

	}
	
	public void CloseConnection(){
		db.Close ();
		db = null;
	}
}
