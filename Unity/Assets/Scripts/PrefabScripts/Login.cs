﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class Login : Page 
{


	public UIInput txt_LoginName;
	public UIInput txt_Password;

	GameObject mainGameObject;
	public GameObject loginHelp;

	string url;

	void Start () {

	}
	
	public void Submit()
	{

		string value1 =  txt_LoginName.value;//"fpplayerone@yahoo.co.za"; 
		string value2 =  txt_Password.value;//;"hello";

		url = "http://footprintapp.net/script.php?var1=" + value1 + "&var2=" + value2;
		//url = "http://www.google.com";
		WWW www = new WWW(url);
		StartCoroutine(WaitForRequest(www));
		
		//Display.instance.ChangePages ("Profiles");
	}


	IEnumerator WaitForRequest(WWW www)
	{

		yield return www;

		// check for errors
		if (www.error == null)
		{
			//print("WWW Ok!: " + www.text);
			var N = JSONNode.Parse(www.text);

			string message = N["message"];


			//print (message);

			if (message == "LoggedIn")
			{
				PlayerPrefs.SetInt("IsLoggedIn", 1);
				
				mainGameObject = GameObject.Find("1.Main");
				mainGameObject.GetComponent<Main>().Login();
				
				Display.instance.ChangePages ("Profiles");
				string scoutId = N["data"]["id"];
				int theId = int.Parse(scoutId);
			
				PlayerPrefs.SetInt("ScoutId", theId);
				
				//print ("Loggenin");
				//print (PlayerPrefs.GetInt("ScoutId"));

				//PlayerPrefs.SetInt("IsLoggedIn", 1);
			}else{
				loginHelp.SetActive(true);
			}



		} else {
			print("WWW Error: "+ www.error);
		}
	}

	public void OpenFootprint()
	{
		Application.OpenURL ("http:www.footprintapp.net");
	}

	public void Continue()
	{
		Display.instance.ChangePages ("Profiles");

		mainGameObject = GameObject.Find("1.Main");
		mainGameObject.GetComponent<Main>().Login();
	}
}



