﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Data;
using System.Text;
using Mono.Data.SqliteClient;

public class Assessments : Page {
	
	// Match Details
	public int matchId;
	public int currentMatch;
	public String team1Name;
	public String team2Name;
	public int scoutId;
	public string theTeam;
	
	//match buttons
	public GameObject PrefabMatchButton;
	public GameObject matchButtonHolder;
	
	// Player Assessment
	public String sildersName;
	public String sliderValue;
	public String theSliderName;
	public String sliderTeam;
	

	// Buttons
	public GameObject[] team1OrangeDots;
	public GameObject[] team2GreenDots;
	public int buttonNumber;
	int buttonNumbers;
	int numPlayers = 23;

	//Page Setup
	public UILabel match;
	public UILabel grounds;
	public UILabel city;
	public UILabel conditions;
	
	// read, write and save data
	private SqliteConnection db = null;
	public string filePath;
	public string result;
	private string connection;
	private IDbConnection dbcon;
	private IDbCommand dbcmd;
	private IDataReader reader;
	private StringBuilder builder;
	

	void Start()
	{	
		buttonNumber = 0;
		LoadMatch();
		ChangeTeam1();
		ChangeTeam2();
	}
	
	void ChangeMatch()
	{
		//PlayerPrefs.SetInt("currentMatch", matchId);
		//LoadMatch();
	}
	
	public void LoadMatch()
	{
		GetMatches ();
		
		//Populate match buttons
		for(int i = 0; i < matches.Count; i++)
		{
			GameObject loadedPrefab = Resources.Load("MatchButton") as GameObject;
			NGUITools.AddChild(matchButtonHolder, loadedPrefab);
			string theText = matches[i].Team1 + " VS " + matches[i].Team2;
			matchButtonHolder.transform.GetChild(i).GetComponent<MatchButton>().buttonTxt.text = theText;
			matchButtonHolder.transform.GetChild(i).GetComponent<MatchButton>().matchID = matches[i].ID;
		}
		
		
		// Populate Match info
		scoutId = PlayerPrefs.GetInt ("ScoutId");
		currentMatch = PlayerPrefs.GetInt("CurrentMatch");
		SetMatches (currentMatch);
		
		match.text = matches [0].Team1 + " VS " + matches [0].Team2; 
		grounds.text = matches [0].Venu;
		city.text = matches [0].City;
		conditions.text = matches [0].Conditions;
		
		team1Name = matches [0].Team1;
		team2Name = matches [0].Team2;
	}
	
	public void GetMatches()
	{
		if (matches == null)
			matches = new List<Match> ();
		if (matches.Count > 0)
			matches.Clear ();
		
		//db = new SqliteConnection ("URI=file:C:/wamp/www/ScoutData.db");
		
		ConnectSql ();
		try {
			////All Teams
			string sql = "SELECT * FROM matches";
			
			// select where
			//string sql = "SELECT * FROM Teams WHERE TeamID = 2";
			
			SqliteDataReader reader = Exicute(sql);
			
			if (reader.HasRows) {
				while (reader.Read()) {
					Match l = new Match ();
					l.ID = int.Parse (reader ["matchID"].ToString ());
					l.Team1 = reader ["homeTeam"].ToString ();
					l.Team2 = reader ["awayTeam"].ToString ();
					matches.Add (  l  );
				}
			} else {
				// ADD TEAMS
			}
			reader.Close ();
			reader = null;
		} catch {
			print ("Not Connected");
		}
	}	
	
	public void SliderControl() // updates sliders
	{
		ChooseTeams();
		//print (sliderTeam);
		//print (theSliderName);
		//print (sliderValue);
		//print (buttonNumber);
		
		buttonNumbers = buttonNumber + 1;
		ConnectSql ();
		try {
			////All Teams
			string sql = "SELECT * FROM assessments WHERE matchId = '"+ currentMatch +"' AND scoutId = '"+ scoutId + "' AND positionNumber = '"+ buttonNumbers + "' AND team = '"+ theTeam +"'";


			// select where
			//string sql = "SELECT * FROM Teams WHERE TeamID = 2";
			
			SqliteDataReader reader = Exicute(sql);
			
			if (reader.HasRows) {
				bool ok = ExicuteQuery ("UPDATE assessments SET "+theSliderName+" = "+sliderValue+" WHERE matchId = '"+ currentMatch +"' AND scoutId = '"+ scoutId + "' AND positionNumber = '"+ buttonNumbers + "' AND team = '"+ theTeam +"'");
				//print ("Value updated");
			} else {

				bool ok = ExicuteQuery ("INSERT INTO assessments (matchId, scoutId, team, positionNumber,  "+theSliderName+" ) VALUES('" + currentMatch + "','" + scoutId + "','" + theTeam + "','" + buttonNumbers + "','" + sliderValue + "')");
			}
			reader.Close ();
			reader = null;
		} catch {
			print ("Not Connected");
		}
	}

	private bool ExicuteQuery(string sql)
	{
		ConnectSql();
		try{
			SqliteCommand cmd = new SqliteCommand (sql , db);
			int rowId = cmd.ExecuteNonQuery ();
			if (rowId > 0)
			{
				return true;
			}else{
				return false;
			}
			
		}catch {
			print ("Query not exicuted");
			return false;
		}
		CloseConnection();
	}

	//////////////////////////////////////////////////////////////////
	
	List <Match> matches;
	public struct Match
	{
		public int ID;
		public string Team1;
		public string Team2;
		public string Venu;
		public string City;
		public string Conditions;
	}
	
	public void SetMatches(int matchId)
	{
	
	
		if (matches == null)
			matches = new List<Match> ();
		if (matches.Count > 0)
			matches.Clear ();
		
		//db = new SqliteConnection ("URI=file:C:/wamp/www/ScoutData.db");
		
		ConnectSql ();
		try {
			////All Teams
			string sql = "SELECT * FROM matches WHERE matchId = '"+ matchId +"'";
			
			// select where
			//string sql = "SELECT * FROM Teams WHERE TeamID = 2";
			
			SqliteDataReader reader = Exicute(sql);
			
			if (reader.HasRows) {
				while (reader.Read()) {
					Match l = new Match ();
					l.ID = int.Parse (reader ["matchID"].ToString ());
					l.Team1 = reader ["homeTeam"].ToString ();
					l.Team2 = reader ["awayTeam"].ToString ();
					l.Venu = reader ["venu"].ToString ();
					l.City = reader ["city"].ToString ();
					l.Conditions = reader ["conditions"].ToString ();

					matches.Add (  l  );
				}
			} else {
				// ADD TEAMS
			}
			reader.Close ();
			reader = null;
			CloseConnection();
		} catch {
			print ("Not Connected");
		}
	}	
	
	
	
	// read data
	private SqliteDataReader Exicute(string sql){
		ConnectSql ();
		try {
			SqliteCommand cmd = new SqliteCommand (sql, db);
			SqliteDataReader reader = cmd.ExecuteReader ();
			
			return reader;
			
		}catch {
			print ("Not Connected");
			return null;
		}
		CloseConnection();
	}

	public void ConnectSql(){
		string p = "ScoutDataTest2.db";
		string filepath = Application.persistentDataPath + "/" + p;
		
		//print (filepath);
		
		if(!File.Exists(filepath))
		{
			// if it doesn't ->
			// open StreamingAssets directory and load the db ->
			WWW loadDB = new WWW("jar:file://" + Application.dataPath + "!/assets/" + p);  // this is the path to your StreamingAssets in android
			while(!loadDB.isDone) {}  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
			
			// then save to Application.persistentDataPath
			
			File.WriteAllBytes(filepath, loadDB.bytes);
			
		}
		
		//open db connection
		connection = "URI=file:" + filepath;
		db = new SqliteConnection(connection);
		db.Open();
	}
	
	public void CloseConnection(){
		db.Close ();
		db = null;
	}

	public void ChangeNumberTeam1()
	{
		Invoke ("ChangeTeam1", 0.2f);
	}

	public void ChangeNumberTeam2()
	{
		Invoke ("ChangeTeam2", 0.2f);
	}

	void ChangeTeam1()
	{
		for (int i = 0; i < numPlayers; i++) 
		{
			team1OrangeDots[i].SetActive(false);
		}
		
		
		team1OrangeDots[buttonNumber].SetActive(true);
	}

	void ChangeTeam2()
	{
		for (int i = 0; i < numPlayers; i++) 
		{
			team2GreenDots[i].SetActive(false);
		}
		
		team2GreenDots[buttonNumber].SetActive(true);
	}
	
	void ChooseTeams()
	{
		if(sliderTeam == "Team1")
		{
			theTeam = matches [0].Team1;
		}
		else
		{
			theTeam = matches [0].Team2; 
		}
	}
	
}
