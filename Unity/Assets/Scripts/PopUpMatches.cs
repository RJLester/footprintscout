﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Data;
using System.Text;
using Mono.Data.SqliteClient;

public class PopUpMatches : Page 
{
	private SqliteConnection db = null;

	private string connection;
	private IDbConnection dbcon;
	private IDbCommand dbcmd;
	private IDataReader reader;
	private StringBuilder builder;
	
	public UIPopupList popUpList;
	
	void Start () {
		getMatches ();
		
		//Populate match buttons
		for(int i = 0; i < matches.Count; i++)
		{
			//GameObject loadedPrefab = Resources.Load("MatchButton") as GameObject;
			//NGUITools.AddChild(matchButtonHolder, loadedPrefab);
			
			string theText = matches[i].Team1 + " VS " + matches[i].Team2;
			popUpList.items.Add(theText);
			
			
			
			//matchButtonHolder.transform.GetChild(i).GetComponent<MatchButton>().buttonTxt.text = theText;
			//matchButtonHolder.transform.GetChild(i).GetComponent<MatchButton>().matchID = matches[i].ID;
		}
	}
	
	public void MatchSelected()
	{
		print ("clicked");
		//print (popUpList.items.IndexOf);
	}
	
	
	//////////////////////////////////////////////////////////////////
	
	List <Match> matches;
	public struct Match
	{
		public int ID;
		public string Team1;
		public string Team2;
	}
	
	public void getMatches()
	{
		if (matches == null)
			matches = new List<Match> ();
		if (matches.Count > 0)
			matches.Clear ();
		
		//db = new SqliteConnection ("URI=file:C:/wamp/www/ScoutData.db");
		
		ConnectSql ();
		try {
			////All Teams
			string sql = "SELECT * FROM matches";
			
			// select where
			//string sql = "SELECT * FROM Teams WHERE TeamID = 2";
			
			SqliteDataReader reader = Exicute(sql);
			
			if (reader.HasRows) {
				while (reader.Read()) {
					Match l = new Match ();
					l.ID = int.Parse (reader ["matchID"].ToString ());
					l.Team1 = reader ["homeTeam"].ToString ();
					l.Team2 = reader ["awayTeam"].ToString ();
					matches.Add (  l  );
				}
			} else {
				// ADD TEAMS
			}
			reader.Close ();
			reader = null;
		} catch {
			print ("Not Connected");
		}
	}	
	
	
	
	// read data
	private SqliteDataReader Exicute(string sql){
		ConnectSql ();
		try {
			SqliteCommand cmd = new SqliteCommand (sql, db);
			SqliteDataReader reader = cmd.ExecuteReader ();
			
			return reader;
			
		}catch {
			print ("Not Connected");
			return null;
		}
		CloseConnection();
	}
	
	public void ConnectSql(){
		string p = "ScoutDataTest2.db";
		string filepath = Application.persistentDataPath + "/" + p;
		
		if(!File.Exists(filepath))
		{
			// if it doesn't ->
			// open StreamingAssets directory and load the db ->
			WWW loadDB = new WWW("jar:file://" + Application.dataPath + "!/assets/" + p);  		// this is the path to your StreamingAssets in android
			while(!loadDB.isDone) {}  															// CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
			
			// then save to Application.persistentDataPath
			
			File.WriteAllBytes(filepath, loadDB.bytes);
			
		}
		
		//open db connection
		print (filepath);
		connection = "URI=file:" + filepath;
		db = new SqliteConnection(connection);
		db.Open();
		
	}
	
	public void CloseConnection(){
		db.Close ();
		db = null;
	}
}
