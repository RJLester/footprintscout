﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Data;
using System.Text;
using Mono.Data.SqliteClient;


public class ScoreOffOn : MonoBehaviour {
	public UILabel theScore;
	int currentMatch;
	
	private SqliteConnection db = null;
	private string connection;
	
	string team1Half;
	string team2Half;
	string team1Full;
	string team2Full;
	
	GameObject main;
	// Use this for initialization
	void Start () {
		main = GameObject.Find("1.Main");
		currentMatch = PlayerPrefs.GetInt("CurrentMatch");
		GetGameScore();
	}
	
	public void GetGameScore()
	{
		currentMatch = PlayerPrefs.GetInt("CurrentMatch");
		
		ConnectSql ();
		try {
			
			string sql = "SELECT * FROM matches WHERE matchId = '"+ currentMatch +"'";
			
			SqliteDataReader reader = Exicute(sql);
			
			if (reader.HasRows) {
				int i = 0;
				
				while (reader.Read()) {
					if(i==0){
						if (reader ["homeHalfTime"] != null){
							team1Half = reader ["homeHalfTime"].ToString ();
						}
						if (reader ["homeFullTime"] != null){
							team2Half = reader ["homeFullTime"].ToString ();
						}	
						if (reader ["awayHalfTime"] != null){
							team1Full = reader ["awayHalfTime"].ToString ();
						}
						if (reader ["awayFullTime"] != null){
							team2Full = reader ["awayFullTime"].ToString ();
						}
					}
					i++;	
				}
				
			} else {
				// ADD TEAMS
			}
			
			theScore.text = team1Half + " / " + team2Half + " - " + team1Full + " / " + team2Full;
			
			reader.Close ();
			reader = null;
			CloseConnection();
		} catch {
			print ("Not Connected");
		}
	}	
	
	
	
	
	public void turnOnOff()
	{
		main.GetComponent<Main>().ScoreControl();
	}
	
	private bool ExicuteQuery(string sql)
	{
		ConnectSql();
		try{
			SqliteCommand cmd = new SqliteCommand (sql , db);
			int rowId = cmd.ExecuteNonQuery ();
			if (rowId > 0)
			{
				return true;
			}else{
				return false;
			}
			
		}catch {
			print ("Query not exicuted");
			return false;
		}
		CloseConnection();
	}
	
	
	// read data
	private SqliteDataReader Exicute(string sql){
		ConnectSql ();
		try {
			SqliteCommand cmd = new SqliteCommand (sql, db);
			SqliteDataReader reader = cmd.ExecuteReader ();
			
			return reader;
			
		}catch {
			print ("Not Connected");
			return null;
		}
		CloseConnection();
	}
	
	public void ConnectSql(){
		string p = "ScoutDataTest2.db";
		string filepath = Application.persistentDataPath + "/" + p;
		
		//print (filepath);
		
		if(!File.Exists(filepath))
		{
			// if it doesn't ->
			// open StreamingAssets directory and load the db ->
			WWW loadDB = new WWW("jar:file://" + Application.dataPath + "!/assets/" + p);  // this is the path to your StreamingAssets in android
			while(!loadDB.isDone) {}  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
			
			// then save to Application.persistentDataPath
			
			File.WriteAllBytes(filepath, loadDB.bytes);
			
		}
		
		//open db connection
		connection = "URI=file:" + filepath;
		db = new SqliteConnection(connection);
		db.Open();
	}
	
	public void CloseConnection(){
		db.Close ();
		db = null;
	}
}
