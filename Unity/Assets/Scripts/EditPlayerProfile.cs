﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using System.Data;
using System.Text;
using Mono.Data.SqliteClient;

public class EditPlayerProfile : MonoBehaviour {
	public UIInput playerName;
	public UIInput surname;
	public UIInput id;
	//public UIInput union;
	public UIInput devision;
	public UIInput ageGroup;
	public UIInput team;
	public UIInput position;
	public UIInput age;
	
	public UILabel mental;
	public UILabel law;
	public UILabel cond;
	public UILabel attack;
	public UILabel def;
	public UILabel kickGame;
	public UILabel breakDown;
	public UILabel scrum;
	public UILabel lineOut;
	public UILabel kickRecieve;

	public int currentMatch;
	public int scoutId;
	int playerAssessId;
	
	public GameObject mainPage;
	
	public GameObject team1;
	public GameObject team2;

	private SqliteConnection db = null;
	private string connection;
	
	
	public void GetPlayerDetails()
	{
		//print ("Getting Data!!!!!!");
		mainPage = GameObject.Find("1.Main");
		playerAssessId = mainPage.GetComponent<Main>().playerAssessId;
		print (playerAssessId);
		
		//db = new SqliteConnection ("URI=file:C:/wamp/www/ScoutData.db");
		
		ConnectSql ();
		try {
			
			string sql = "SELECT * FROM assessments WHERE assessId = '"+ playerAssessId +"'";
			//string sql = "SELECT * FROM Teams WHERE TeamID = 2";
			
			SqliteDataReader reader = Exicute(sql);
			
			if (reader.HasRows) {
				//print (playerNo);
				int i = 0;
				//playerName.text = "Name";
				//surname.text = "Surname";
				//id.text = "Position";
				//lableAge.text = "Age";

				
				while (reader.Read()) {
					if(i==0){
						if (reader ["playerName"] != null){
							playerName.value = reader ["playerName"].ToString ();
						}
						if (reader ["playerSurname"] != null){
							surname.value = reader ["playerSurname"].ToString ();
						}	
						if (reader ["id"] != null){
							id.value = reader ["id"].ToString ();
						}
						if (reader ["devision"] != null){
							devision.value = reader ["devision"].ToString ();
						}
						if (reader ["devision"] != null){
							devision.value = reader ["devision"].ToString ();
						}
						
						if (reader ["ageGroup"] != null){
							ageGroup.value = reader ["ageGroup"].ToString ();
						}
						
						if (reader ["team"] != null){
							team.value = reader ["team"].ToString ();
						}
						
						if (reader ["position"] != null){
							position.value = reader ["position"].ToString ();
						}
						
						if (reader ["age"] != null){
							age.value = reader ["age"].ToString ();
						}
						
						/// ////////////////////////////////////////////////
						
//						if (reader ["mantalApp"] != null && float.Parse (reader ["mantalApp"].ToString ()) > 0 ){
//							
//							uISlider_mental.value = (float.Parse (reader ["mantalApp"].ToString ())) / 10;
//							
//						}
//						if (reader ["lawApp"] != null){
//							uISlider_law.value = (float.Parse (reader ["lawApp"].ToString ())) / 10;
//							
//						}
//						if (reader ["conditioning"] != null){
//							uISlider_cond.value = (float.Parse (reader ["conditioning"].ToString ())) / 10;
//						}
//						if (reader ["attack"] != null){
//							uISlider_attack.value = (float.Parse (reader ["attack"].ToString ())) / 10;
//						}
//						if (reader ["defence"] != null){
//							uISlider_def.value = (float.Parse (reader ["defence"].ToString ())) / 10;
//						}
//						if (reader ["kickingGame"] != null){
//							uISlider_kickGame.value = (float.Parse (reader ["kickingGame"].ToString ())) / 10;
//						}
//						if (reader ["breakDown"] != null){
//							uISlider_breakDown.value = (float.Parse (reader ["breakDown"].ToString ())) / 10;
//						}
//						if (reader ["scrum"] != null){
//							uISlider_scrum.value = (float.Parse (reader ["scrum"].ToString ())) / 10;
//						}
//						if (reader ["lineout"] != null){
//							uISlider_lineOut.value = (float.Parse (reader ["lineout"].ToString ())) / 10;
//						}
//						if (reader ["kickRecieved"] != null){
//							uISlider_kickRecieve.value = (float.Parse (reader ["kickRecieved"].ToString ())) / 10;
//						}
					}
					
					i++;	
					
				}
				
				
				
			} else {
				// ADD TEAMS
			}
			reader.Close ();
			reader = null;
			CloseConnection();
		} catch {
			print ("Not Connected");
		}
	}	



	public void UpdateDetails()
	{
		ConnectSql ();
		try {
			////All Teams
			string sql = "SELECT * FROM assessments WHERE assessId = '"+ playerAssessId +"'";
			
			
			// select where
			//string sql = "SELECT * FROM Teams WHERE TeamID = 2";
			
			SqliteDataReader reader = Exicute(sql);
			
			
			if (reader.HasRows) {
				bool ok = ExicuteQuery ("UPDATE assessments SET playerName = '"+playerName.value+"', playerSurname = '"+surname.value+"', id = '"+id.value+"', devision = '"+devision.value+"', ageGroup = '"+ageGroup.value+"', team = '"+team.value+"', position = '"+position.value+"', age = '"+age.value+"' WHERE assessId = '"+ playerAssessId +"'");
				print ("Value updated");
			} else {
				bool ok = ExicuteQuery ("INSERT INTO assessments (playerName, playerSurname, id, devision, ageGroup, team, position, age) VALUES('" + playerName.value + "','" + surname.value + "','" + id.value + "','" + devision.value + "','" + ageGroup.value + "','" + team.value + "','" + position.value + "','" + age.value + "')");
				print ("Value edited");
			}
			reader.Close ();
			reader = null;
		} catch {
			print ("Not Connected");
		}
	
	
		ResetAllDetails();
		
			team1 = GameObject.Find("Team1");
			team2 = GameObject.Find("Team2");
			team1.GetComponent<PlayerAssessment>().ChangePlayer();
			team2.GetComponent<PlayerAssessment>().ChangePlayer();
			
		gameObject.SetActive(false);
	}
	
	private bool ExicuteQuery(string sql)
	{
		ConnectSql();
		try{
			SqliteCommand cmd = new SqliteCommand (sql , db);
			int rowId = cmd.ExecuteNonQuery ();
			if (rowId > 0)
			{
				return true;
			}else{
				return false;
			}
			
		}catch {
			print ("Query not exicuted");
			return false;
		}
		CloseConnection();
	}
	
	public void Close()
	{
		ResetAllDetails();
		mainPage.GetComponent<Main>().profileIsOn = false;
		gameObject.SetActive(false);
	}
	
	// read data
	private SqliteDataReader Exicute(string sql){
		ConnectSql ();
		try {
			SqliteCommand cmd = new SqliteCommand (sql, db);
			SqliteDataReader reader = cmd.ExecuteReader ();
			
			return reader;
			
		}catch {
			print ("Not Connected");
			return null;
		}
		CloseConnection();
	}
	
	public void ConnectSql(){
		string p = "ScoutDataTest2.db";
		string filepath = Application.persistentDataPath + "/" + p;
		
		//print (filepath);
		
		if(!File.Exists(filepath))
		{
			// if it doesn't ->
			// open StreamingAssets directory and load the db ->
			WWW loadDB = new WWW("jar:file://" + Application.dataPath + "!/assets/" + p);  // this is the path to your StreamingAssets in android
			while(!loadDB.isDone) {}  // CAREFUL here, for safety reasons you shouldn't let this while loop unattended, place a timer and error check
			
			// then save to Application.persistentDataPath
			
			File.WriteAllBytes(filepath, loadDB.bytes);
			
		}
		
		//open db connection
		connection = "URI=file:" + filepath;
		db = new SqliteConnection(connection);
		db.Open();
	}
	
	public void CloseConnection(){
		db.Close ();
		db = null;
	}
	
	void ResetAllDetails()
	{
		
		playerName.value = "Name";
		surname.value = "Surname";
		id.value =  "ID";
		devision.value = "Devision";
		ageGroup.value = "Age Group";
		team.value = " Team";
		position.value =  "Position";
		age.value = "Age";
		
	}
}
