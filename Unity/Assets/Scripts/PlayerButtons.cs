﻿using UnityEngine;
using System.Collections;

public class PlayerButtons : MonoBehaviour {

	public GameObject assessments;
	public GameObject theTeam;
	public GameObject blackObject;
	
	int buttonNumber;

	void OnClick()
	{
		blackObject.SetActive (false);
		
		string buttonName = gameObject.name;
		buttonNumber = int.Parse(buttonName.Substring (4));
		assessments.GetComponent<Assessments>().buttonNumber = buttonNumber - 1;
		//print (buttonNumber);
		theTeam.GetComponent<PlayerAssessment>().playerNo = buttonNumber;
		theTeam.GetComponent<PlayerAssessment> ().ChangePlayer ();
		
		
	}


//	public void OnActivate () 
//	{
//		if (!isSelected) 
//		{
//			selectedImage.SetActive(false);
//			isSelected = true;
//		} 
//		else 
//		{
//			selectedImage.SetActive(true);
//			isSelected = false;
//		}			
//	}
}
